

// Support routines for Hamamatsu C14272 and C13272 IR tunable sensors

#include "serial.h"
#include "defines.h"
#include "eeprom.h"


// these calibration constants come from Hamamatsu (with each sensor)
#define C13272_0  -1.693021924E+04
#define C13272_1  0
#define C13272_2  4.217803258E+02
#define C13272_3  -4.571122581E+01
#define C13272_4  2.224709597E+00
#define C13272_5  -5.766432199E-02
#define C13272_6  7.775288452E-04
#define C13272_7  -4.310499175E-06

#define C13272_MAX 1850
#define C13272_MIN 1550
#define C13272_MIN_V 25.994      
#define C13272_MAX_V 37.763      

// these calibration constants come from measuring the circuits
const double C13272_calibration=20.61;      // gain provided by amplifier - measure this, should be around 20
const double C13272_offset=0.0;             // offset provided by amplifier - measure this, should be around 0


// Same for C14272

// these calibration constants come from Hamamatsu (with each sensor)
#define C14272_0  4.274316506E+03
#define C14272_1  -1.063348250E+03
#define C14272_2  1.848638350E+02
#define C14272_3  -1.770308327E+01
#define C14272_4  1.004634031E+00
#define C14272_5  -3.387096405E-02
#define C14272_6  6.285611361E-04
#define C14272_7  -4.964679787E-06


#define C14272_MAX 1650
#define C14272_MIN 1350
#define C14272_MIN_V 11.519      
#define C14272_MAX_V 27.141      

// these calibration constants come from measuring the circuits
const double C14272_calibration=20.61;      // gain provided by amplifier - measure this, should be around 20
const double C14272_offset=0.0;             // offset provided by amplifier - measure this, should be around 0



// given a desired lambda (in nm), find the needed dac value to set the filter to this lamda
// note: higher voltage yields lower lamda

// C14272 is the first channel/detector

int hamC14272(int lamda)
{
  if (lamda > C14272_MAX) 
     return 0;
  if (lamda < C14272_MIN)
     return 0;
     
  // iterively try all values that the DAC can produce (quick and dirty)
  // return the best one

  float lamda_error_min = 99999;
  int dac_best = 0;

  for (int dac = 0; dac < 4096; ++dac)
  {
      float V = dac/4096.0 * 2.048 * C14272_calibration + C14272_offset;   // internal ref voltage is 2.048V

      if (V < C14272_MIN_V) continue;
      
      float calc_lamda = C14272_0 + C14272_1*V + C14272_2*(V*V) + C14272_3*(V*V*V) + C14272_4*(V*V*V*V) + C14272_5*(V*V*V*V*V) + C14272_6*(V*V*V*V*V*V) + C14272_7*(V*V*V*V*V*V*V);
      float calc_error = fabs(lamda - calc_lamda);

      //Serial_Printf("%g %d %g\n",V, dac, calc_lamda);
       
      if (calc_error < lamda_error_min) {   // a better match?
         lamda_error_min = calc_error;
         dac_best = dac;
      }

      // early exit
      if (V > C14272_MAX_V)
         break;
      
  } // for

//  Serial_Printf("best dac fit is %i\n", dac_best);
  
  return dac_best;
  
}  // hamC14272()


//====================================================


int hamC13272(int lamda)
{
  if (lamda > C13272_MAX) 
     return 0;
  if (lamda < C13272_MIN)
     return 0;
     
  // iterively try all values that the DAC can produce (quick and dirty)
  // return the best one

  float lamda_error_min = 99999;
  int dac_best = 0;

  for (int dac = 0; dac < 4096; ++dac)
  {
      float V = dac/4096.0 * 2.048 * C13272_calibration + C13272_offset;   // internal ref voltage is 2.048V

      if (V < C13272_MIN_V) continue;
      
      float calc_lamda = C13272_0 + C13272_1*V + C13272_2*(V*V) + C13272_3*(V*V*V) + C13272_4*(V*V*V*V) + C13272_5*(V*V*V*V*V) + C13272_6*(V*V*V*V*V*V) + C13272_7*(V*V*V*V*V*V*V);
      float calc_error = fabs(lamda - calc_lamda);

      //Serial_Printf("%g %d %g\n",V, dac, calc_lamda);
       
      if (calc_error < lamda_error_min) {   // a better match?
         lamda_error_min = calc_error;
         dac_best = dac;
      }

      // early exit
      if (V > C13272_MAX_V)
         break;
      
  } // for

//  Serial_Printf("best dac fit is %i\n", dac_best);
  
  return dac_best;
  
}  // hamC13272()

