
//  Jon Zeeff
//  version supporting single internal DAC and multiplexot

#define LDAC1 0
#define LDAC2 0
#define LDAC3 0

// DAC functions
int DAC_init(void);
void DAC_shutdown(void);
void DAC_change(void);
void DAC_set(unsigned int LED, unsigned int value);
int DAC_set_address(int ldac_pin, unsigned oldAddress, unsigned newAddress); 

void MEM_setup();
void MEM_value(int chan, int value);
