
// Many variables used have to be stored/retrieved from eeprom.  All such variables are defined in this structure.
// This structure is stored directly in eeprom memory
// Assume that there was a calibration process that initialized the eeprom.

// DANGER:  do not do too many writes to eeprom variables - cycles are limited
// WARNING: max size is 2K
// WARNING: Write only with the store() function.  If not, program may freeze.

// Jon Zeeff 2016

// this structure overlays eeprom ram

#include <stdint.h>

const int NUM_USERDEFS = 70;

class eeprom_class
{
  public:
    volatile long device_id;          // the 4 upper bytes of bluetooth mac address
    volatile long device_manufacture; // month and year, eg 12016
    volatile long shutdownTime = 7 * 60 * 1000;  // shutdown time for device by default set ot 7 minutes

    // DEPRECIATED 
    /*
    volatile float mag_bias[3];       // magnetometer/compass calibration
    volatile float mag_cal[3][3];
    volatile float accel_bias[3];
    volatile float accel_cal[3][3];   // accelerometer calibration
    */

    //   DEPRECIATED now we calculate par based on rgb components of the incoming light and a yintercept
    //     volatile float light_slope;
    /*
    volatile float light_yint;         // y intercept for par calibration from tcs
    volatile float light_slope_all;    // slope for the total intensity value for par calibration from tcs
    volatile float light_slope_r;      // slope for just the red portion of the part calibration from tcs
    volatile float light_slope_g;      // " " just green
    volatile float light_slope_b;      // " " just blue
    */
    volatile float detector_offset_slope[4];      // slope and yint for detector offsets (detector 1 - 4 as array positions 1 - 4)
    volatile float detector_offset_yint[4];
    /*
       DEPRECIATED calibration for leaf thickness (3 parameters - a, b, and c, plus min and max values)
    volatile float thickness_a;
    volatile float thickness_b;
    volatile float thickness_c;
    volatile float thickness_min;
    volatile float thickness_max;
    */
    /*
       To ensure comparability betweeon devices, create a black and shiney object standard.  All lights are run across both detectors (IR --> detector 0, vis --> detector 1) for black and shiney to get the calibration.  In addition, a blank can be run
    */
    volatile float ir_high[NUM_LEDS + 1];
    volatile float ir_low[NUM_LEDS + 1];
    volatile float ir_high_master[NUM_LEDS + 1];
    volatile float ir_low_master[NUM_LEDS + 1];
    volatile float vis_high[NUM_LEDS + 1];
    volatile float vis_low[NUM_LEDS + 1];
    volatile float vis_high_master[NUM_LEDS + 1];
    volatile float vis_low_master[NUM_LEDS + 1];
    volatile float ir_blank[NUM_LEDS + 1];
    volatile float vis_blank[NUM_LEDS + 1];
    volatile float heatcal1[NUM_LEDS + 1];
    volatile float heatcal2[NUM_LEDS + 1];
    volatile float heatcal3[NUM_LEDS + 1];
//    volatile float heatcal4[NUM_LEDS + 1];
//    volatile float heatcal5[NUM_LEDS + 1];

    /*
       DEPRECIATED IR baseline estimation based on the reflectivity of the object.  This helps address any residual IR in the measuring light itself (used in chlorophyll fluorescence).
    */
//    volatile float ir_baseline_yint[NUM_LEDS + 1];
    /*
       DEPRECIATED values for color calibrations (used for SPAD, anthocyanin, etc.) at 3 intensity levels (allows for simpler protocols as thickness doesn't need to be adjusted or on the fly).
    volatile float colorcal_blank1[NUM_LEDS + 1];       // blank used to calculate absorbance for LEDs 1,2,3,4,6,8,9,10 .  Blank here is actual blank.
    volatile float colorcal_blank2[NUM_LEDS + 1];       // same as colorcal_blank1, but this blank is a single piece of white paper (for thicker samples)
    volatile float colorcal_blank3[NUM_LEDS + 1];       // same as colorcal_blank1, but this blank is 3 pieces of white paper stacked (for even thicker samples)
    */

    // Created from firmware 3.1.1 to store normalization data.
    volatile float normalized_soil[NUM_LEDS + 1];
    volatile float normalized_5mlcuvette[NUM_LEDS + 1];
    volatile float normalized_1mlcuvette[NUM_LEDS + 1];
    volatile float normalized_custom[NUM_LEDS + 1];


/*
    volatile float colorcal_intensity1_slope[NUM_LEDS + 1]; // slope from calibration to Minolta SPAD at intensity 1 (use colorcal_blank1 as blank)
    volatile float colorcal_intensity1_yint[NUM_LEDS + 1]; // y intercept from calibration to Minolta SPAD at intensity 1 (use colorcal_blank1 as blank)
    volatile float colorcal_intensity2_slope[NUM_LEDS + 1]; // slope from calibration to Minolta SPAD at intensity 2 (use colorcal_blank1 as blank)
    volatile float colorcal_intensity2_yint[NUM_LEDS + 1]; // y intercept from calibration to Minolta SPAD at intensity 2 (use colorcal_blank1 as blank)
    volatile float colorcal_intensity3_slope[NUM_LEDS + 1]; // slope from calibration to Minolta SPAD at intensity 3 (use colorcal_blank1 as blank)
    volatile float colorcal_intensity3_yint[NUM_LEDS + 1]; // y intercept from calibration to Minolta SPAD at intensity 3 (use colorcal_blank1 as blank)
*/
    // user set values
    volatile float userdef[NUM_USERDEFS + 1];

    volatile char sleep;     // TODO set to 1 to sleep on boot
};

// where to store permanent data (teensy 3 specific)
#define FlexRAM ((eeprom_class *)0x14000000)

#ifndef EXTERN
extern class eeprom_class * eeprom;
#else
class eeprom_class * eeprom = FlexRAM;
#endif

#undef EXTERN
