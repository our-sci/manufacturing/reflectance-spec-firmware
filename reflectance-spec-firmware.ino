// includes
#include "defines.h"
// #include <i2c_t3.h>
// #include <Time.h>                   // enable real time clock library
#define EXTERN
#include "eeprom.h"
#include "serial.h"
#include <SPI.h>
#include "util.h"
#include <TimeLib.h>

void setup_pins(void);          // initialize pins

// This routine is called first

void setup()
{
  
  // turn on power and initialize ICs

  // Set up I2C bus - CAUTION: any subsequent calls to Wire.begin() will mess this up
//  Wire.begin(I2C_MASTER, 0x00, I2C_PINS_18_19, I2C_PULLUP_INT, I2C_RATE_800);  // using alternative wire library

  turn_on_3V3();                 // note: these routines already have delay() in them

  //  delay(100);                   // let battery voltage stabilize - if there are no other delays ahead of it then this can be deleted.

  if (eeprom->sleep == 1) {     // sleep forever if requested
    store(sleep, 0);           // but don't sleep after next reboot
    deep_sleep();
  }

  // set up serial ports (Serial and Serial1)
  Serial_Set(4);                // auto switch between USB and BLE
  Serial_Begin(115200);

  turn_on_5V();                  // LEAVE THIS HERE!  Lots of hard to troubleshoot problems emerge if this is removed.
  delay(5000);

  // set up MCU pins
  setup_pins();

  // initialize SPI bus
  SPI.begin ();

  eeprom_initialize();      // eeprom
  assert(sizeof(eeprom_class) < 2048);      // check that we haven't exceeded eeprom space
  setTime(Teensy3Clock.get());              // set time from RTC
  Serial_Print(DEVICE_NAME);                // note: this may not display because Serial isn't ready
  Serial_Print_Line(" Ready");

  // testing BME
  // Serial_Print("Testing BME");
  // bme.begin(0x76);
  // Serial_Print("ran bme.begin");
  // temperature = bme.readTemperature();  // temperature in C
  // Serial_Print("measured temperature: ");
  // Serial_Print_Line(temperature);

}  // setup() - now execute loop()


void setup_pins()
{
  pinMode(13, OUTPUT);
 
  // set up LED on/off pins
  for (unsigned i = 1; i < NUM_LEDS + 1; ++i)
    pinMode(LED_to_pin[i], INPUT);              // actually an output, but let pullup resistor work

  // pins used to turn on/off detector integration/discharge
  pinMode(HOLDM, OUTPUT);
  digitalWriteFast(HOLDM, HIGH);                  // discharge cap

  // pin for actopulser/LED on/off
  pinMode(PULSE_OFF, OUTPUT);

  // ADC inputs
  pinMode(A1_P, INPUT); //Diff Channel Positive
  pinMode(A1_N, INPUT); //Diff Channel Negative

  pinMode(A2_P, INPUT); //Diff Channel Positive
  pinMode(A2_N, INPUT); //Diff Channel egative

}


void unset_pins()     // save power, set pins to high impedance
{
  // turn off almost every pin
  for (unsigned i = 0; i < 33; ++i)
    //     if (i != 18 && i != 19 && i != WAKE_DC && i != WAKE_3V3 && i != 0 && i != 1)  // leave I2C and power control on
    pinMode(i, INPUT);
}
