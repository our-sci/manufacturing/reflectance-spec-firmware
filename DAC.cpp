//  This is used to set LED intensity

// Version using single intermal teensy DAC and a multiplexor.
// Teensy DAC seems to output 1.2V max or 3.2V max, depending on analog reference setting

//  Do not write to the DACs anywhere else.
//  Any calls from outside this file assume that pins are numbered 1-x (vs 0-x)
//  Jon Zeeff, March, 2016

#include "DAC.h"
#include "serial.h"
#include "defines.h"

// initialize the DACs

int DAC_init(void)
{
  pinMode(INTENSITY, OUTPUT);
  analogWriteResolution(12);
  analogWrite(INTENSITY, 0);  // set DAC value to control LED intensity
  return 0;
}  // DAC_init()



// shutdown the DAC to save power
void DAC_shutdown()
{

}

// set the DAC value for a particular LED (eg, 1-10)
// and select that LED
// value is 0-4095 (12 bits)

void DAC_set(unsigned int led, unsigned int value)
{
  if (led ==  0 || led > NUM_LEDS)                                            // if you get a zero, quietly skip it
    return;

  // turn off all LED pins (probably already off)
  for (int i = 1; i <= NUM_LEDS; ++i) {
    int pin = LED_to_pin[i];
    pinMode(pin, INPUT);          // allow pullup to work
  }
  
  if (value > 0) {
    // turn on specified LED
    int pin = LED_to_pin[led];
    pinMode(pin, OUTPUT);
    digitalWrite(pin, LOW);         // pull down  
  //  Serial.printf("set pin %d\n",pin);
  }
    
  analogWrite(INTENSITY, value);  // set DAC value to control LED intensity

}  // DAC_set()

// Cause DAC changes to take effect
// This allows many changes to happen at the same time

void DAC_change(void)
{


} // DAC_change()


// re-address a dac IC

int DAC_set_address(int ldac_pin, unsigned oldAddress, unsigned newAddress)           // ldac pin and address to assign (0-7)
{
  int ret = 0;

  return ret;

}  // DAC_set_address()


//----------------------------------------------------------------------------------

// Routines for DAC/MEM control

//#include "src/mcp4728.h"

//static mcp4728 dac = mcp4728(0); // instantiate mcp4728 object, Device ID = 0

void MEM_setup()
{
  // mcp4728 dac = mcp4728(0); // instantiate mcp4728 object, Device ID = 0
  
  // dac.begin();  // initialize i2c interface
  // dac.vdd(3300); // set VDD(mV) of MCP4728 for correct conversion between LSB and Vout

  // //  If LDAC pin is not grounded, need to be pull down for normal operation.
  // int LDACpin = 6;
  // pinMode(LDACpin, OUTPUT); 
  // digitalWrite(LDACpin, LOW);
  
  // dac.setVref(1,1,1,1);     // set to use internal voltage reference (2.048V)
  // dac.setGain(0,0,0,0);     // set the gain of internal voltage reference ( 0 = gain x1, 1 = gain x2 )
  
}

void MEM_value(int chan, int value)
{
  // mcp4728 dac = mcp4728(0); // instantiate mcp4728 object, Device ID =  = 0x60

  // dac.begin();
  // delay(100);
  // //dac.setGain(0,0,0,0);     // set the gain of internal voltage reference ( 0 = gain x1, 1 = gain x2 )
  // delay(100);
  
  // if (chan != 0 && chan != 1) return;
  // if (value < 0 || value > 4095) return;

  // dac.analogWrite(chan,value); // write to input register of a DAC. Channel 0-3, Value 0-4095

  // delay(100);
}




 


